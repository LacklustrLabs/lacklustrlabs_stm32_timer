/**
 * Based on data from RM0008 Reference manual:
 *  STM32F101xx, STM32F102xx, STM32F103xx, STM32F105xx and STM32F107xx advanced ARM ® -based 32-bit MCUs
 */
#include <TimerDiagnostics_STM.h>

#if defined(ARDUINO_ARCH_STM32F1) // TODO: this is not the correct maple core identifier
#include <HardwareTimer.h>

lacklustrlabs::stm32timers::AdvancedTimer timer1Diagnostics(::HardwareTimer(1).c_dev()->regs.adv);
lacklustrlabs::stm32timers::GeneralTimer timer2Diagnostics(::HardwareTimer(2).c_dev()->regs.gen);
lacklustrlabs::stm32timers::GeneralTimer timer3Diagnostics(::HardwareTimer(3).c_dev()->regs.gen);
lacklustrlabs::stm32timers::GeneralTimer timer4Diagnostics(::HardwareTimer(4).c_dev()->regs.gen);
#ifdef STM32_HIGH_DENSITY
lacklustrlabs::stm32timers::GeneralTimer timer5Diagnostics(::HardwareTimer(5).c_dev()->regs.gen);
lacklustrlabs::stm32timers::BasicTimer timer6Diagnostics(::HardwareTimer(6).c_dev()->regs.basic);
lacklustrlabs::stm32timers::BasicTimer timer7Diagnostics(::HardwareTimer(7).c_dev()->regs.basic);
lacklustrlabs::stm32timers::AdvancedTimer timer8Diagnostics(::HardwareTimer(8).c_dev()->regs.adv);
#endif

#endif
